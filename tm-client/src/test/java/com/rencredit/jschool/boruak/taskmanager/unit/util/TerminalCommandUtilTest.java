package com.rencredit.jschool.boruak.taskmanager.unit.util;

import com.rencredit.jschool.boruak.taskmanager.constant.ArgumentConst;
import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class TerminalCommandUtilTest {

    @Test
    public void testFormatBytesHelp() {
        Assert.assertEquals(TerminalConst.HELP, TerminalCommandUtil.convertArgumentToCommand(ArgumentConst.HELP));
    }

    @Test
    public void testFormatBytesExit() {
        Assert.assertEquals(TerminalConst.EXIT, TerminalCommandUtil.convertArgumentToCommand(ArgumentConst.EXIT));
    }

    @Test
    public void testFormatBytesUnknownArgument() {
        Assert.assertEquals("UnknownArgument", TerminalCommandUtil.convertArgumentToCommand("UnknownArgument"));
    }

}
