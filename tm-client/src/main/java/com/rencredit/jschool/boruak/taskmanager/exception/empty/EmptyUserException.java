package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyUserException extends AbstractClientException {

    public EmptyUserException() {
        super("Error! User is empty...");
    }

}
