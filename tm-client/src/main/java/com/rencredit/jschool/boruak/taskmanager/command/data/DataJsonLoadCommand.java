package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataJsonLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws IOException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyElementsException_Exception, EmptyIdException_Exception, EmptyHashLineException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception {
        System.out.println("[DATA JSON LOAD]");
        @NotNull final SessionDTO session = serviceLocator.getSystemObjectService().getSession();
        endpointLocator.getAdminEndpoint().loadJson(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
