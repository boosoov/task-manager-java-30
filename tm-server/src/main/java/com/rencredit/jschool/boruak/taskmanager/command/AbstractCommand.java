package com.rencredit.jschool.boruak.taskmanager.command;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Objects;

public abstract class AbstractCommand {

    @NotNull
    protected IEndpointLocator endpointLocator;

    @NotNull
    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setEndpointLocator(
            @NotNull IEndpointLocator endpointLocator
    ) {
        this.endpointLocator = endpointLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute() throws IOException, ClassNotFoundException, EmptyCommandException, EmptyUserException, EmptyProjectException, EmptyTaskException, EmptySessionException;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractCommand command = (AbstractCommand) o;
        return endpointLocator.equals(command.endpointLocator) &&
                serviceLocator.equals(command.serviceLocator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(endpointLocator, serviceLocator);
    }

    @Override
    public String toString() {
        return "AbstractCommand{" +
                "endpointLocator=" + endpointLocator +
                ", serviceLocator=" + serviceLocator +
                '}';
    }

}
