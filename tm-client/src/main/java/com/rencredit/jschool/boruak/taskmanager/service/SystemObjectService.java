package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISystemObjectService;
import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SystemObjectService implements ISystemObjectService {

    @NotNull
    private final ISystemObjectRepository systemObjectRepository;

    public SystemObjectService(@NotNull final ISystemObjectRepository systemObjectRepository) {
        this.systemObjectRepository = systemObjectRepository;
    }

    @Nullable
    @Override
    public SessionDTO putSession(@Nullable final SessionDTO session) throws EmptySessionException {
        if(session == null) throw new EmptySessionException();
        @Nullable final SessionDTO lastSession = systemObjectRepository.putSession(session);
        return lastSession;
    }

    @Nullable
    @Override
    public SessionDTO getSession() {
        @Nullable final SessionDTO session = systemObjectRepository.getSession();
        return session;
    }

    @Nullable
    @Override
    public SessionDTO removeSession() {
        @Nullable final SessionDTO session = systemObjectRepository.removeSession();
        return session;
    }

    @Override
    public void clearAll() {
        systemObjectRepository.clearAll();
    }

}
