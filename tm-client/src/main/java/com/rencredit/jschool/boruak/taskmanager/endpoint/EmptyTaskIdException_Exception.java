
package com.rencredit.jschool.boruak.taskmanager.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-01T19:14:23.162+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "EmptyTaskIdException", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/")
public class EmptyTaskIdException_Exception extends Exception {

    private com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyTaskIdException emptyTaskIdException;

    public EmptyTaskIdException_Exception() {
        super();
    }

    public EmptyTaskIdException_Exception(String message) {
        super(message);
    }

    public EmptyTaskIdException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public EmptyTaskIdException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyTaskIdException emptyTaskIdException) {
        super(message);
        this.emptyTaskIdException = emptyTaskIdException;
    }

    public EmptyTaskIdException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyTaskIdException emptyTaskIdException, java.lang.Throwable cause) {
        super(message, cause);
        this.emptyTaskIdException = emptyTaskIdException;
    }

    public com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyTaskIdException getFaultInfo() {
        return this.emptyTaskIdException;
    }
}
