package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyHashLineException extends AbstractException {

    public EmptyHashLineException() {
        super("Error! Line for hash is empty...");
    }

}
