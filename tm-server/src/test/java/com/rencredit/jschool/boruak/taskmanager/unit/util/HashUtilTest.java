package com.rencredit.jschool.boruak.taskmanager.unit.util;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyHashLineException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class HashUtilTest {

    @Test
    public void getHashLineTest() throws EmptyHashLineException {
        Assert.assertEquals("fcbe8b302d6b1a50d5886fee0f108bf6", HashUtil.getHashLine("line"));
    }

    @Test(expected = EmptyHashLineException.class)
    public void testNegativeSaltHashLineWithoutLine() throws EmptyHashLineException {
        HashUtil.saltHashLine(null);
    }

    @Test
    public void saltHashLineTest() throws EmptyHashLineException {
        Assert.assertEquals("fcbe8b302d6b1a50d5886fee0f108bf6", HashUtil.saltHashLine("line"));
    }

    @Test(expected = EmptyHashLineException.class)
    public void testNegativeHashLineMD5WithoutLine() throws EmptyHashLineException {
        HashUtil.hashLineMD5(null);
    }

    @Test
    public void hashLineMD5Test() throws EmptyHashLineException {
        Assert.assertEquals("6438c669e0d0de98e6929c2cc0fac474", HashUtil.hashLineMD5("line"));
    }

}
