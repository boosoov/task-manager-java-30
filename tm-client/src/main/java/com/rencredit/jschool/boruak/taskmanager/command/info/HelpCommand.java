package com.rencredit.jschool.boruak.taskmanager.command.info;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import org.jetbrains.annotations.NotNull;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal command.";
    }

    @Override
    public void execute() throws EmptyCommandException {
        System.out.println("[HELP]");
        System.out.println(serviceLocator.getInfoService().getHelp());
    }

}
