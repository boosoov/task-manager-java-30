package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.command.info.SystemInfoCommand;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.CommandRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Map;

@Category(UnitTestCategory.class)
public class CommandRepositoryTest {

    @Test
    public void testPutCommand() {
        @NotNull final CommandRepository commandRepository = new CommandRepository();
        Assert.assertEquals(0, commandRepository.getCommands().length);
        @NotNull final AbstractCommand command = new SystemInfoCommand();

        commandRepository.putCommand(command.name(), command);
    }

    @Test
    public void testGetCommands() {
        @NotNull final CommandRepository commandRepository = new CommandRepository();
        @NotNull final AbstractCommand command = new SystemInfoCommand();
        commandRepository.putCommand(command.name(), command);

        @NotNull final String[] commands = commandRepository.getCommands();
        Assert.assertEquals(1, commands.length);
        Assert.assertEquals(command.name() + ": " + command.description(), commands[0]);
    }

    @Test
    public void testGetArgs() {
        @NotNull final CommandRepository commandRepository = new CommandRepository();
        @NotNull final AbstractCommand command = new SystemInfoCommand();
        commandRepository.putCommand(command.name(), command);

        @NotNull final String[] args = commandRepository.getArgs();
        Assert.assertEquals(1, args.length);
        Assert.assertEquals(command.arg() + ": " + command.description(), args[0]);
    }

    @Test
    public void testGetTerminalCommands() {
        @NotNull final CommandRepository commandRepository = new CommandRepository();
        @NotNull final AbstractCommand command = new SystemInfoCommand();
        commandRepository.putCommand(command.name(), command);

        @NotNull final Map<String, AbstractCommand> commands = commandRepository.getTerminalCommands();
        Assert.assertEquals(1, commands.size());
        @NotNull final AbstractCommand commandFromRepository = commands.get(command.name());
        Assert.assertEquals(command, commandFromRepository);
    }

}
