package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.Collection;

public class AbstractRepository<E extends AbstractEntityDTO> implements IRepository<E> {

    @NotNull final EntityManager em;

    public AbstractRepository() {
        this.em = EntityManagerFactoryUtil.getEntityManager();
    }

    public void begin() {
        em.getTransaction().begin();
    }

    public void commit() {
        em.getTransaction().commit();
    }

    public void rollback() {
        em.getTransaction().rollback();
    }

    public void close() {
        em.close();
    }

    public void add(@NotNull final E record) {
        em.persist(record);
    }

    public void remove(@NotNull final E record) {
        em.remove(record);
    }

    public void clearAll() {
        em.clear();
    }

    public void load(@NotNull final Collection<E> records) {
        clearAll();
        merge(records);
    }

    public void load(@NotNull final E... records) {
        clearAll();
        merge(records);
    }

    public void merge(@NotNull final Collection<E> records) {
        for (@NotNull final E record : records) {
            em.merge(record);
        }
    }

    public void merge(@NotNull final E... records) {
        for (@NotNull final E record : records) {
            em.merge(record);
        }
    }

    public void merge(@NotNull final E record) {
        em.merge(record);
    }

}
