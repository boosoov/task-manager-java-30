package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.bootstrap.Bootstrap;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.io.IOException;

public interface IAdminEndpoint {

    @WebMethod
    String getHostPort(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    String getHost(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    Integer getPort(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, ClassNotFoundException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException;

    @WebMethod
    boolean saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, ClassNotFoundException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException;

    @WebMethod
    boolean saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean cleanJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException;

    @WebMethod
    boolean saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    boolean loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException;

    @WebMethod
    boolean saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    void shutDownServer(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

}
