package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
public class AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
